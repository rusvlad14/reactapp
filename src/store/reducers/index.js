import { combineReducers } from "redux";
import userReducer from "./userReducer";
import itemsReducer from "./itemsReducer";
import confirmationModal from "./confirmationModal";

const rootReducer = combineReducers({
  userReducer,
  itemsReducer,
  confirmationModal,
});

export default rootReducer;
