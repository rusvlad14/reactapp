export default {
  userReducer: {
    token: null,
  },
  itemsReducer: {
    items: [],
  },
  confirmationModal: {
    isOpen: false,
    confirmationFunction: () => {},
    message: "CONFIRM?",
  },
};
